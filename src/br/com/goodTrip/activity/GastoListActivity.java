package br.com.goodTrip.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;
import android.widget.Toast;
import br.com.goodTrip.R;
import br.com.goodTrip.dao.GoodTripDAO;
import br.com.goodTrip.model.Constantes;
import br.com.goodTrip.model.Gasto;
import br.com.goodTrip.model.Viagem;

public class GastoListActivity extends ListActivity
		implements OnItemClickListener{
	
	private List<Map<String, Object>> gastos;
	private String dataAnterior = "";
	private Viagem viagem;
	private GoodTripDAO dao;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String[] de = { "data", "descricao", "valor", "categoria" };
		int[] para = { R.id.data, R.id.descricao,
		R.id.valor, R.id.categoria };
		
		if(getIntent()!=null && getIntent().getExtras()!=null){
			Long idDaViagem = getIntent().getLongExtra(Constantes.VIAGEM_ID, 1l);
			viagem = new Viagem(idDaViagem);
		}
		SimpleAdapter adapter = new SimpleAdapter(this, listarGastos(), R.layout.lista_gasto, de, para);

		adapter.setViewBinder(new GastoViewBinder());
		setListAdapter(adapter);
		getListView().setOnItemClickListener(this);
		registerForContextMenu(getListView());
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.configuracoes_menu, menu);
		return true;
	}
	
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
			case R.id.volta_telaprincipal:
				startActivity(new Intent(this, DashBoardActivity.class));
				return true;
			default:
				return super.onMenuItemSelected(featureId, item);
		}
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.gasto_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.remover_gasto) {
			dao = new GoodTripDAO(this);
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
			Long idDoGasto = (Long) gastos.get(info.position).get("id");
			boolean removeuGastoDoBanco = dao.removerGasto(idDoGasto);
			if(removeuGastoDoBanco)
				gastos.remove(info.position);
			getListView().invalidateViews();
//			dataAnterior = "";
			return true;
		}
		return super.onContextItemSelected(item);
	}



	private List<Map<String, Object>> listarGastos() {
		
		dao = new GoodTripDAO(this);
		List<Gasto> listarGastos = dao.listarGastos(viagem);
		dao.close();
		
		Map<String, Object> item;
		gastos = new ArrayList<Map<String, Object>>();
		for (Gasto gasto : listarGastos) {
			item = new HashMap<String, Object>();
			item.put("id", gasto.getId());
			item.put("data", gasto.getData());
			item.put("descricao", gasto.getDescricao());
			item.put("valor", gasto.getValor());
			item.put("categoria", gasto.getColorDaCategoria());
			gastos.add(item);
		}
		return gastos;
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		Map<String, Object> map = gastos.get(position);
		String descricao = (String) map.get("descricao");
		String mensagem = "Gasto selecionada: " + descricao;
		Toast.makeText(this, mensagem,Toast.LENGTH_SHORT).show();
	}
	
	
	private class GastoViewBinder implements ViewBinder {
		@Override
		public boolean setViewValue(View view, Object data, String textRepresentation) {
			if(view.getId() == R.id.data){
				if(!dataAnterior.equals(data)){
					TextView textView = (TextView) view;
					textView.setText(textRepresentation);
					dataAnterior = textRepresentation;
					view.setVisibility(View.VISIBLE);
				} else {
					view.setVisibility(View.GONE);
				}
				return true;
			}
			if(view.getId() == R.id.categoria){
				Integer id = (Integer) data;
				view.setBackgroundColor(getResources().getColor(id));
				return true;
			}
			return false;
		}
	}


}
