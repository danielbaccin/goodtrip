package br.com.goodTrip.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import br.com.goodTrip.calendar.CalendarService;
import br.com.goodTrip.model.Viagem;

public class CalendarTask extends AsyncTask<Viagem, Void, String>{
	
	private Context context;
	private ProgressDialog progress;
	private String idDoEvento;
	private CalendarService calendarService;
	
	
	public CalendarTask(Context context, CalendarService calendarService){
		this.context = context;
		this.calendarService = calendarService;
	}
	
	@Override
	protected void onPreExecute() {
		progress = ProgressDialog.show(context, "Aguarde...", "Criando evendo no Google Calendar...",true, true);
	}
	
	@Override
	protected String doInBackground(Viagem... viagens) {
		Viagem viagem = viagens[0];
		idDoEvento = calendarService.criarEvento(viagem);
		viagem.setIdDoEventoNoGoogleAgenda(idDoEvento);
		return idDoEvento;
	}
	
	@Override
	protected void onPostExecute(String result) {
		progress.dismiss();
		Toast.makeText(context, "id do evento criado: " + result, Toast.LENGTH_LONG).show();
	}

}
