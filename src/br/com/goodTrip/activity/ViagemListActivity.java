package br.com.goodTrip.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.Toast;
import br.com.goodTrip.R;
import br.com.goodTrip.dao.GoodTripDAO;
import br.com.goodTrip.helper.DatabaseHelper;
import br.com.goodTrip.model.Constantes;
import br.com.goodTrip.model.Viagem;

public class ViagemListActivity extends ListActivity implements OnItemClickListener, OnClickListener, ViewBinder{
	
	
	private List<Map<String, Object>> viagens;
	private AlertDialog alertDialog;
	private int viagemSelecionada;
	private AlertDialog dialogConfirmacao;
	private boolean modoSelecionarViagem;
	private DatabaseHelper helper;
	private SimpleDateFormat dateFormat;
	private Double valorLimite;
	private GoodTripDAO dao;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		helper = new DatabaseHelper(this);
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(this);
		String valor = preferencias.getString("valor_limite", "-1");
		valorLimite = Double.valueOf(valor);

		getListView().setOnItemClickListener(this);
		this.alertDialog = criaAlertDialog();
		this.dialogConfirmacao = criaDialogConfirmacao();
		
		if (getIntent().hasExtra(Constantes.MODO_SELECIONAR_VIAGEM)) {
			modoSelecionarViagem = getIntent().getExtras().getBoolean(Constantes.MODO_SELECIONAR_VIAGEM);
		}
		
		new Task().execute();

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.viagem_list_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemClicado = item.getItemId();
		switch (itemClicado) {
		case R.id.volta_telaprincipal:
			startActivity(new Intent(this, DashBoardActivity.class));
			return false;
		default:
            return super.onOptionsItemSelected(item);
		}
		
	}
	
	private List<Map<String, Object>>  listarViagens() {
		dao = new GoodTripDAO(this);
		List<Viagem> viagensDoDao = dao.listarViagens();
		viagens = new ArrayList<Map<String, Object>>();
		Map<String, Object> item = null;
		for (Viagem viagem : viagensDoDao) {
			item = new HashMap<String, Object>();
			item.put("id", viagem.getId());
			if (viagem.isLazer()) {
				item.put("imagem", R.drawable.lazer);
			} else {
				item.put("imagem", R.drawable.negocios);
			}
			item.put("destino", viagem.getDestino());
			String periodo = dateFormat.format(viagem.getDataChegada()) + " a "	+ dateFormat.format(viagem.getDataSaida());
			item.put("data", periodo);
			
			double totalGasto = dao.calcularTotalGasto2(viagem);
			item.put("total", "Gasto total R$ " + totalGasto);
			double alerta = viagem.getOrcamento() * valorLimite / 100;
			Double [] valores =
			new Double[] { viagem.getOrcamento(), alerta, totalGasto };
			item.put("barraProgresso", valores);
			viagens.add(item);
			
		}
		dao.close();
		return viagens;
	}

	

		
	

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (modoSelecionarViagem) {
			String destino = (String) viagens.get(position).get("destino");
			Long idViagem = (Long) viagens.get(position).get("id");

			Intent intent = new Intent(this, GastoActivity.class);
			intent.putExtra(Constantes.VIAGEM_ID, idViagem);
			intent.putExtra(Constantes.VIAGEM_DESTINO, destino);
			startActivity(intent);
		} else {
			viagemSelecionada = position;
			alertDialog.show();
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int item) {
		Intent intent;
		Long id = (Long) viagens.get(viagemSelecionada).get("id");
		String destino = (String) viagens.get(viagemSelecionada).get("destino");

		switch (item) {
			case 0:
				intent = new Intent(this, ViagemActivity.class);
				intent.putExtra(Constantes.VIAGEM_ID, id.toString());
				startActivity(intent);
				break;

			case 1:
				intent = new Intent(this, GastoActivity.class);
				intent.putExtra(Constantes.VIAGEM_ID, id);
				intent.putExtra(Constantes.VIAGEM_DESTINO, destino);
				startActivity(intent);
				break;
			case 2:
				intent = new Intent(this, GastoListActivity.class);
				intent.putExtra(Constantes.VIAGEM_ID, id);
				startActivity(intent);
				break;
			case 3:
				dialogConfirmacao.show();
				break;
			case DialogInterface.BUTTON_POSITIVE:
				viagens.remove(viagemSelecionada);
				removerViagem(id.toString());
				getListView().invalidateViews();
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialogConfirmacao.dismiss();
				break;

		}

		
	}
	
	@Override
	protected void onDestroy() {
		helper.close();
		super.onDestroy();
	}
	
	private void removerViagem(String id) {
		dao = new GoodTripDAO(this);
		boolean removeuViagem= false;
		dao.removerGastosDaViagem(Long.valueOf(id));
		dao.removerViagem(Long.valueOf(id));
		dao.close();
		if(removeuViagem){
			Toast.makeText(this, getString(R.string.registro_removido),
					Toast.LENGTH_SHORT).show();
		}
		
	}

	private AlertDialog criaAlertDialog() {
		final CharSequence[] items = {
			getString(R.string.editar),
			getString(R.string.novo_gasto),
			getString(R.string.gastos_realizados),
			getString(R.string.remover) 
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.opcoes);
		builder.setItems(items, this);
		return builder.create();
	}

	
	private AlertDialog criaDialogConfirmacao() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.confirmacao_exclusao_viagem);
		builder.setPositiveButton(getString(R.string.sim), this);
		builder.setNegativeButton(getString(R.string.nao), this);
		return builder.create();
	}

	@Override
	public boolean setViewValue(View view, Object data, String textRepresentation) {
		if (view.getId() == R.id.barraProgresso) {
			Double valores[] = (Double[]) data;
			ProgressBar progressBar = (ProgressBar) view;
			progressBar.setMax(valores[0].intValue());
			progressBar.setSecondaryProgress(valores[1].intValue());
			progressBar.setProgress(valores[2].intValue());
			return true;
		}
		return false;
	}
	
	
	private class Task extends AsyncTask<Void, Void, List<Map<String, Object>>>{
		@Override
		protected List<Map<String, Object>> doInBackground(Void... params) {
			return listarViagens();
		}
		@Override
		protected void onPostExecute(List<Map<String, Object>> result) {
			String[] de = { "imagem", "destino", "data", 
					"total", "barraProgresso" };
	
			int[] para = { R.id.tipoViagem, R.id.destino, 
				   R.id.data, R.id.valor,
				   R.id.barraProgresso };
	
			SimpleAdapter adapter = new SimpleAdapter(ViagemListActivity.this, result, R.layout.lista_viagem, de, para);
			adapter.setViewBinder(ViagemListActivity.this);
			setListAdapter(adapter);
		}
	}



}
