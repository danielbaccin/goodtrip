package br.com.goodTrip.activity;

import java.io.IOException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import br.com.goodTrip.R;
import br.com.goodTrip.model.Constantes;

import com.google.api.client.googleapis.extensions.android.accounts.GoogleAccountManager;

public class LoginActivity extends Activity{
	
	private CheckBox manterConectado;
	private EditText usuario;
	private EditText senha;
	
	private SharedPreferences preferencias;
	private Account conta;
	private GoogleAccountManager accountManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		accountManager = new GoogleAccountManager(this);
		
		usuario = (EditText) findViewById(R.id.usuario);
		senha = (EditText) findViewById(R.id.senha);
		manterConectado = (CheckBox) findViewById(R.id.manterConectado);
		
		preferencias = getSharedPreferences(Constantes.PREFERENCIAS, MODE_PRIVATE);
		boolean conectado = preferencias.getBoolean(Constantes.MANTER_CONECTADO, false);
		
		if(conectado){
			solicitarAutorizacao();
		}

	}
	
	public void entrarOnClick(View v) {
		String usuarioInformado = usuario.getText().toString();
		String senhaInformada = senha.getText().toString();
		
		autenticar(usuarioInformado, senhaInformada);

	}

	private void autenticar(String nomeConta, String senhaInformada) {

		conta = accountManager.getAccountByName(nomeConta);
		if(conta == null){
			Toast.makeText(this, R.string.conta_inexistente,Toast.LENGTH_LONG).show();
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putString(AccountManager.KEY_ACCOUNT_NAME, nomeConta);
		bundle.putString(AccountManager.KEY_PASSWORD, senhaInformada);
		accountManager.getAccountManager().confirmCredentials(conta , bundle, this,	new AutenticacaoCallback(), null);
	}
	
	
	private class AutenticacaoCallback implements AccountManagerCallback<Bundle> {
		@Override
		public void run(AccountManagerFuture<Bundle> future) {
			try {
				Bundle bundle = future.getResult();
				if(bundle.getBoolean(AccountManager.KEY_BOOLEAN_RESULT)) {
					solicitarAutorizacao();
				} else {
					Toast.makeText(getBaseContext(), getString(R.string.erro_autenticao),Toast.LENGTH_LONG).show();
				}
			} catch (OperationCanceledException e) {
				// usuário cancelou a operação
			} catch (AuthenticatorException e) {
				// possível falha no autenticador
			} catch (IOException e) {
				// possível falha de comunicação
			}
		}
	}
	
	private class AutorizacaoCallback implements AccountManagerCallback<Bundle> {
		
		@Override
		public void run(AccountManagerFuture<Bundle> future) {
			// TODO Auto-generated method stub
			try {
				Bundle bundle = future.getResult();
				String nomeConta = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
				String tokenAcesso = bundle.getString(AccountManager.KEY_AUTHTOKEN);
				
				gravarTokenAcesso(nomeConta, tokenAcesso);
				iniciarDashboard();
				
				} catch (OperationCanceledException e) {
				// usuário cancelou a operação
				} catch (AuthenticatorException e) {
				// possível problema no autenticador
				} catch (IOException e) {
					// possível problema de comunicação
				}
		}

		private void iniciarDashboard() {
			startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
		}
		

		private void gravarTokenAcesso(String nomeConta, String tokenAcesso) {
			Editor editor = preferencias.edit();
			editor.putString(Constantes.NOME_CONTA, nomeConta);
			editor.putString(Constantes.TOKEN_ACESSO, tokenAcesso);
			editor.commit();
		}

	}
	
	private void solicitarAutorizacao() {
		String tokenAcesso = preferencias.getString(Constantes.TOKEN_ACESSO, null);
		String nomeConta = preferencias.getString(Constantes.NOME_CONTA, null);
		if(tokenAcesso != null){
			accountManager.invalidateAuthToken(tokenAcesso);
			conta = accountManager.getAccountByName(nomeConta);
		}
		accountManager.getAccountManager().getAuthToken(conta, Constantes.AUTH_TOKEN_TYPE, null, this, new AutorizacaoCallback(), null);
	}
	
}
