package br.com.goodTrip.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import br.com.goodTrip.R;
import br.com.goodTrip.model.Constantes;

public class DashBoardActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.dashbord_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		finish();
		return true;
	}


	
	
	public void selecionarOpcao(View view) {
		switch (view.getId()) {
			case R.id.nova_viagem:
				startActivity(new Intent(this, ViagemActivity.class));
				break;
				
			case R.id.novo_gasto:
				SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(this);
				boolean modoViagem = preferencias.getBoolean(Constantes.MODO_VIAGEM, false);
				
				if(modoViagem){
					//obter o id da viagem atual
					Intent intent = new Intent(this, GastoActivity.class);
					intent.putExtra(Constantes.VIAGEM_ID, Long.valueOf(1));
					intent.putExtra(Constantes.VIAGEM_DESTINO, "Viagem fixa");
					startActivity(intent);
				}else{
					Intent intent = new Intent(this, ViagemListActivity.class);
					intent.putExtra(Constantes.MODO_SELECIONAR_VIAGEM, true);
					startActivityForResult(intent, 0);
				}
				break;
			case R.id.minhas_viagens:
				startActivity(new Intent(this, ViagemListActivity.class));
				break;
			case R.id.configuracoes:
				startActivity(new Intent(this, ConfiguracoesActivity.class));
				break;

		}
	}


}
