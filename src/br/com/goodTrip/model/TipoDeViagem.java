package br.com.goodTrip.model;

public enum TipoDeViagem {
	
	VIAGEM_LAZER(1, "Viagem de Lazer"), VIAGEM_NEGOCIOS(2, "Viajem de Negocios");
	
	private Integer codigo;
	private String decricao;
	
	private TipoDeViagem(Integer codigo, String descricao){
		this.codigo = codigo;
		this.decricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDecricao() {
		return decricao;
	}
	
}
