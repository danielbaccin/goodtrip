package br.com.goodTrip.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import br.com.goodTrip.R;
import br.com.goodTrip.calendar.CalendarService;
import br.com.goodTrip.dao.GoodTripDAO;
import br.com.goodTrip.helper.DatabaseHelper;
import br.com.goodTrip.model.Constantes;
import br.com.goodTrip.model.TipoDeViagem;
import br.com.goodTrip.model.Viagem;
import br.com.goodTrip.task.CalendarTask;

public class ViagemActivity extends Activity{
	
	private Date dataChegada, dataSaida;
	private int ano, mes, dia;
	private Button dataChegadaButton, dataSaidaButton;
	private DatabaseHelper helper;
	private EditText destino, quantidadePessoas, orcamento;
	private RadioGroup radioGroup;
	private String id;
	private CalendarService calendarService;
	private GoodTripDAO dao;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nova_viagem);
		
		Calendar calendar = Calendar.getInstance();
		ano = calendar.get(Calendar.YEAR);
		mes = calendar.get(Calendar.MONTH);
		dia = calendar.get(Calendar.DAY_OF_MONTH);
		
		dataChegadaButton = (Button) findViewById(R.id.dataChegada);
		dataSaidaButton = (Button) findViewById(R.id.dataSaida);
		
		destino = (EditText) findViewById(R.id.destino);
		quantidadePessoas = (EditText) findViewById(R.id.quantidadePessoas);
		orcamento = (EditText) findViewById(R.id.orcamento);
		radioGroup = (RadioGroup) findViewById(R.id.tipoViagem);

		helper = new DatabaseHelper(this);
		
		id = getIntent().getStringExtra(Constantes.VIAGEM_ID);
		if(id != null){
			prepararEdicao();
		}
		calendarService = criarCalendarService();

	}
	
	
	private CalendarService criarCalendarService() {
		SharedPreferences preferencias = getSharedPreferences(Constantes.PREFERENCIAS, MODE_PRIVATE);
		String nomeConta = preferencias.getString(Constantes.NOME_CONTA, null);
		String tokenAcesso = preferencias.getString(Constantes.TOKEN_ACESSO, null);
		return new CalendarService(nomeConta, tokenAcesso);
	}


	private void prepararEdicao() {
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor cursor =	db.rawQuery("SELECT tipo_viagem, destino, data_chegada, " +
									"data_saida, quantidade_pessoas, orcamento " +
									"FROM viagem WHERE _id = ?", new String[]{ id });
		cursor.moveToFirst();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		if(cursor.getInt(0) == TipoDeViagem.VIAGEM_LAZER.getCodigo()){
			radioGroup.check(R.id.lazer);
		} else {
			radioGroup.check(R.id.negocios);
		}
		destino.setText(cursor.getString(1));
		dataChegada = new Date(cursor.getLong(2));
		dataSaida = new Date(cursor.getLong(3));
		dataChegadaButton.setText(dateFormat.format(dataChegada));
		dataSaidaButton.setText(dateFormat.format(dataSaida));
		quantidadePessoas.setText(cursor.getString(4));
		orcamento.setText(cursor.getString(5));
		cursor.close();

		
	}


	public void selecionarData(View view) {
		showDialog(view.getId());
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case R.id.dataChegada:
			return new DatePickerDialog(this, dataChegadaListener, ano, mes, dia);

		case R.id.dataSaida:
			return new DatePickerDialog(this, dataSaidaListener, ano, mes, dia);
		}
		return null;
	}
	
	private OnDateSetListener dataChegadaListener = new OnDateSetListener() {
		public void onDateSet(DatePicker view, int anoSelecionado, int mesSelecionado, int diaSelecionado) {
			dataChegada = criarData(anoSelecionado, mesSelecionado, diaSelecionado);
			dataChegadaButton.setText(diaSelecionado + "/" + (mesSelecionado + 1) + "/" + anoSelecionado);
		}
	};

	private OnDateSetListener dataSaidaListener = new OnDateSetListener() {
		public void onDateSet(DatePicker view, int anoSelecionado, int mesSelecionado, int diaSelecionado) {
			dataSaida = criarData(anoSelecionado, mesSelecionado, diaSelecionado);
			dataSaidaButton.setText(diaSelecionado + "/" + (mesSelecionado + 1) + "/" + anoSelecionado);
		}
	};
	
	private Date criarData(int anoSelecionado, int mesSelecionado, int diaSelecionado) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(anoSelecionado, mesSelecionado, diaSelecionado);
		return calendar.getTime();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.viagem_menu, menu);
		return true;
	}
	
	@Override
	protected void onDestroy() {
		helper.close();
		super.onDestroy();
	}

	
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
			case R.id.novo_gasto:
				startActivity(new Intent(this, GastoActivity.class));
				return true;
			case R.id.remover_viagem:
				//remover viagem do banco de dados
				return true;
			case R.id.volta_telaprincipal:
				startActivity(new Intent(this, DashBoardActivity.class));
				return true;
			default:
				return super.onMenuItemSelected(featureId, item);
		}
	}
	
	public void salvarViagem(View view){
		dao = new GoodTripDAO(this);
		Viagem viagem = obtemViagem();
		long resultado;
		if(id == null){
			CalendarTask task = new CalendarTask(this, calendarService);
			task.execute(viagem);
			resultado = dao.inserir(viagem);
		}else{
			viagem.setId(Long.valueOf(id));
			resultado = dao.edita(viagem);
		}
		dao.close();
		if(resultado != -1 ){
			Toast.makeText(this, getString(R.string.registro_salvo),
					Toast.LENGTH_SHORT).show();
		}else{
			Toast.makeText(this, getString(R.string.erro_salvar),
			Toast.LENGTH_SHORT).show();
		}
		startActivity(new Intent(this, ViagemListActivity.class));
	}
	

	private Viagem obtemViagem() {
		Viagem viagem = new Viagem(null,
									destino.getText().toString(),
									radioGroup.getCheckedRadioButtonId() == R.id.lazer ? TipoDeViagem.VIAGEM_LAZER.getCodigo(): TipoDeViagem.VIAGEM_NEGOCIOS.getCodigo(),
									dataChegada,
									dataSaida,
									Double.valueOf(orcamento.getText().toString()),
									Integer.valueOf(quantidadePessoas.getText().toString()),
									null);
		return viagem;
	}
	
}
